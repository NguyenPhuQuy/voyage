<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define('DB_NAME', 'smlvoyage');

/** Username của database */
define('DB_USER', 'root');

/** Mật khẩu của database */
define('DB_PASSWORD', '');

/** Hostname của database */
define('DB_HOST', 'localhost');

/** Database charset sử dụng để tạo bảng database. */
define('DB_CHARSET', 'utf8mb4');

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2f67gG-HnCsl6-EsAg]GiPOQSf5TOj=97![IYVT,drl_lvJM?,1.6^X1|wB~}m#:');
define('SECURE_AUTH_KEY',  'gJK{NXIE+8Eb,$oV6)hxP?(h.zU+]6+A}1L(,,47Ng;em;G#gBe#jV[-F>-j4]D%');
define('LOGGED_IN_KEY',    '|AVf)h%[JOXa^{yk<gMiGY]g8!C~xYmH;VN1+T d^NSwHFD@{`$^`4Y1BAL<~95`');
define('NONCE_KEY',        'a`V&2AiqIeh`V>nIe-]W|z8:EOPhd-S[g&u/t~0U(?MOWblO]EUb1+:;Xe2nQo0,');
define('AUTH_SALT',        'z[41 qAC-^3_fuoH|]Rq,2B<M/#3V$Gf%!jLO_q_ L@H2Tf9sfh=zgp3nnaA@<@<');
define('SECURE_AUTH_SALT', 'dp9,q!ghvLl%h/7E:_)/VnLe1%cAeAYd%suxg]UgXEera4C3)EN*TD(XFw?iDyN(');
define('LOGGED_IN_SALT',   'XKN(P,_+$E;U.h#Fp`zt_tZ@8T&SGl>uH_L%HY/#h>ADGRx^JSM@_J{^v8140KOP');
define('NONCE_SALT',       'Aj}AEc$mS&DGJLu#n}8?d#v|q>/g58g*XrEUD?]eMuy;aTXM?b: fL0-|WCMt?b(');

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
