<?php get_header(); ?>
		<main class="wrapper">
      <!-- Breacrum star -->
      <?php custom_breadcrumbs(); ?>
      <!-- Breacrum end -->
      <div class="container">
        <ul class="listPost">
          <?php
            $paged3 = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
            $args3 = array(
              'post_status' => 'publish',
              'posts_per_page' => 5,
              'paged' => $paged3, 
            );
            $the_query3 = new WP_Query( $args3 );

            if ( $the_query3->have_posts() ) :

              while ( $the_query3->have_posts() ) : $the_query3->the_post();

              ?>
              <li class="listPost__item">
                 <p class="datePost"><?php echo get_the_date() .' - '. get_the_time(); ?></p>
                 <?php
                 $categories = get_the_category();
                 if ( ! empty( $categories ) ) {
                     echo '<a class="cat" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                 }
                 ?>
                 <a href="<?php the_permalink(); ?>" class="titlePost"><?php the_title(); ?></a>
              </li>

              <?php
              endwhile;
          endif;
          wp_reset_postdata();
        ?>
      </ul>
    <div class="pagination">
        <?php
           $big = 999999999; 
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
                'current' => max( 1, get_query_var('paged') ),
                'total' => $the_query3->max_num_pages,
                'prev_text' => __ (''),
                'next_text' => __ (''),
            ) );
           wp_reset_postdata();
           ?>
    </div>
  </div>
  </main>
<?php get_footer(); ?>