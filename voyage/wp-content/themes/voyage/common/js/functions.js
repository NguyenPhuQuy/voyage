(function(){

/*===================================================
Index mainvisual
===================================================*/
$('.index__mainvisual').slick({
    infinite: true,
    autoplay:true,
    fade: true,
    autoplaySpeed:4000,
    speed: 1500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" style=""><i class="fa fa-angle-left"></i></button>',
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Previous" type="button" style=""><i class="fa fa-angle-right"></i></button>',
  });
/*===================================================
Index3 content
===================================================*/
$('.index3__content').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
    speed: 1000,
    pauseOnFocus: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
          }
    }
    ]
});
/*===================================================
Index5 content
===================================================*/
$('.index5__content').slick({
    autoplay:true,
    autoplaySpeed: 2000,
    arrows: false,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 2,
        }
      },
    {
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
          }
    }
    ]
});

$(document).ready (function () {
/*===================================================
Add class form
===================================================*/
// Add Class for validate
$('input.form__input--name').addClass('validate[required, [text-input], maxSize[10]]');
$('input.form__input--company').addClass('validate[required, [text-input], maxSize[10]]');
$('input.form__input--mail').addClass('validate[required,custom[email]]');
$('input.form__checkbox').addClass('validate[required] radio');

/*===================================================
scrollDown info index3
===================================================*/
$('.index3__updown').css("display","none");
$('.index3__info').click(function() {
  $(this).find('.index3__updown').slideToggle();
  $(this).toggleClass('is-active');
  if($(this).hasClass('is-active')){
    $('.index3__content').slick('slickPause');
  }else { 
    $('.index3__content').slick('slickPlay');
  }
})

/*===================================================
menu active sp
===================================================*/
   $('.header__menubar').click(function(){
        $('.header__navisp').toggleClass('is-active');
   })

/*===================================================
Button ScrollTop
===================================================*/
   $('.btn__goTop').on('click', function(e) {
      e.preventDefault();
      $('html, boy').animate({
      scrollTop: 0
    }, 800);
  }); 

/*===================================================
Form Validate
===================================================*/
   const _input = $('#form input');
   const _textArea = $('#form textarea');
   const SP_WIDTH = 768;

   _input.on('focus', function() {
      if($(this).val().length === 0){
        $(this).css({borderColor: '#c0272d'})
      }
      $(this).data('placeholder', $(this).attr('placeholder')).attr('placeholder', '');
   }).on('blur', function() {
      $(this).attr('placeholder', $(this).data('placeholder'));
   });
   requireInput(_input);
   requireInput(_textArea);

   function requireInput(input) {
      input.on('keyup', function() {
        let keyUpData = $(this).val().length;
        if(keyUpData === 0) {$(this).css({borderColor: '#c0272d'});}
        else {$(this).css({borderColor: 'transparent'});}
      });
    }

   if($(window).width() < 768) {
      $('#form').validationEngine({
        promptPosition: "topLeft"
      });
   }else {
      $('#form').validationEngine({
        promptPosition: "topRight"
      });
   }

/*===================================================
Add css item
===================================================*/
   $('.index__item').each(function(){
    var _this = $(this);
    _this.css({
      display: 'block',
    })
   })
   
})
}())
   