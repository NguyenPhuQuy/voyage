<?php get_header(); ?>
    <main class="wrapper">
      <!-- Breacrum star -->
      <?php custom_breadcrumbs(); ?>
      <!-- Breacrum end -->
    <div class="container">
      <ul class="listPost">
        <?php
      
          $paged_holiday = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
          $args_holiday = array(
            'post_type' => 'holiday', 
            'posts_per_page' => 10,
            'paged' => $paged_holiday, 
          );
          $the_query_holiday = new WP_Query( $args_holiday );

          if ( $the_query_holiday->have_posts() ) :

            while ( $the_query_holiday->have_posts() ) : $the_query_holiday->the_post();

            ?>
            <li class="listPost__item">
              <p class="datePost"><?php echo get_the_date() .' - '. get_the_time(); ?></p>
              <a href="<?php the_permalink(); ?>" class="titlePost" ><?php the_title(); ?></a>
            </li>
            <?php
            endwhile;
        endif;
        wp_reset_postdata();
      ?>
    </ul>
    <div class="pagination">
        <?php
           $big = 999999999; 
            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
                'current' => max( 1, get_query_var('paged') ),
                'total' => $the_query_holiday->max_num_pages,
                'prev_text' => __ (''),
                'next_text' => __ (''),
            ) );
           wp_reset_postdata();
           ?>
    </div>
  </div>
  </main>
<?php get_footer(); ?>