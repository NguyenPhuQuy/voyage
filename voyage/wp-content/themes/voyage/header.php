<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta property="og:title" content=VOYAGE />
	<meta property="og:url" content=/index.html />
	<meta property="og:type" content=website>
	<meta property="og:site_name" content=VOYAGE />
	<meta property="og:image" content="" />
	<meta property="og:image:width" content="" />
	<meta property="og:image:height" content="" />
	<meta property="og:description" content="" />
	<title><?php single_post_title(); ?></title>
	<link rel="canonical" href=/index.html />
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/common/css/common.css" rel="stylesheet">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/top.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/common/css/responsive.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/common/css/slick.css">
  <link href="<?php echo get_stylesheet_directory_uri(); ?>/common/css/jquery.datepicker.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/common/css/validationEngine.jquery.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>
<body>
  <!--▼ Header ▼-->
  <header class="header">
    <!-- Header Top star -->
    <div class="header__top">
      <div class="container">
        <!-- Header contact star -->
        <div class="header__contact">
          <ul>
            <li><a href="#"><i class="fa fa-user"></i>Agent Login</a></li>
            <li><a href="#"><i class="fa fa-user"></i>Customer Login</a></li>
            <li><a href="#"><i class="fa fa-file-o"></i>Not a Member ? <span class="__regis">Register</span></a></li>
            <li><a href="tel:815-123-4567">Call Us Now :815-123-4567</a></li>
          </ul>
        </div>
        <!-- Header contact end -->
        <!-- Header social star -->
        <div class="header__social">
          <ul>
            <li ><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a></li>
            <li><a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
          </ul>
        </div>
        <!-- Header social end -->
      </div>
    </div>
    <!-- Header Top end -->
    <!-- Header Bottom star -->
    <div class="header__bottom">
      <div class="container">
        <div class="header__menubar">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <h1 class="header__logo"><a href="<?php echo esc_url( home_url( '/home' ) ); ?>">V<span>o</span>yage</a></h1>
        <nav class="header__navi">
          <?php
            wp_nav_menu(array(
              'menu' => 'Menu_Top',
              'theme_location' => 'menu-top',
              'menu_id'        => 'primary-menu',
            ))
          ?>
          <div id="header__search">
              <form action="<?php echo home_url(); ?>" id="search-form" method="get">
                <input type="text" class="header__input" name="s" id="s" value="Search" onblur="if(this.value=='')this.value='Search'"
          onfocus="if(this.value=='Search')this.value=''" />
                <button class="header__btn">
                  <i class="fa fa-search"></i>
                </button>
              </form>
            </div>
        </nav>
        <div class="header__navisp">
          <?php
            wp_nav_menu(array(
              'menu' => 'Menu_Top',
              'theme_location' => 'menu-top',
              'menu_id'        => 'primary-menu',
            ))
          ?>
          <form>
            <input type="text" class="header__input" name="s" id="s" value="Search" onblur="if(this.value=='')this.value='Search'"
      onfocus="if(this.value=='Search')this.value=''" />
            <button class="header__btn">
              <i class="fa fa-search"></i>
            </button>
          </form>
        </div>
      </div>
    </div>
    <!-- Header Bottom end -->
  </header>
  <!--▲ Header ▲-->