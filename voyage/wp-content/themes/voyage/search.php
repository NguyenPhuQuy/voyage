<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<header class="page-header">
	<div class="container">
		<h1>
		<?php

		printf( esc_html__( 'SEARCH RESULTS FOR: %s', 'voyage' ), '<span>' . get_search_query() . '</span>' );
		?>
		</h1>
	</div>
</header>
<main class="wrapper">
	<div class="container">
		<ul class="listPost">
			<?php
				while ( have_posts() ) :
					the_post();

					?>
					<li class="listPost__item">
		                 <p class="datePost"><?php echo get_the_date() .' - '. get_the_time(); ?></p>
		                 <?php
		                 $categories = get_the_category();
		                 if ( ! empty( $categories ) ) {
		                     echo '<a class="cat" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
		                 }
		                 ?>
		                 <a href="<?php the_permalink(); ?>" class="titlePost"><?php the_title(); ?></a>
	              	</li>
					<?php

				endwhile;

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>
		  </ul>
	</div>
</main>

<?php get_footer(); ?>