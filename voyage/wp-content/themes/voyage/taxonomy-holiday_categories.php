
<?php get_header(); ?>
	<main class="wrapper">
		<!-- Breacrum star -->
		<?php custom_breadcrumbs(); ?>
		<!-- Breacrum end -->
		<div class="container">
			<!-- Title star -->
			<h2 class="title__cat">
				<?php $term = get_queried_object();
				echo $term->slug; 
				?>		
			</h2>
			<!-- Title end -->
			<ul class="listPost">
				<?php

		          if (have_posts() ) :

		          while (have_posts() ) : the_post();

		          ?>
		          <li class="listPost__item">
		          	<p class="datePost"><?php echo get_the_date() .' - '. get_the_time(); ?></p>
		          	<?php 
					$terms = get_terms('holiday_categories');
					if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
				        echo '<a href="' . get_term_link( $term ) . '" class="cat">' . $term->name . '</a>';
					}
				 	?>
	          		<a href="<?php the_permalink(); ?>" class="titlePost"><?php the_title(); ?></a>
		          </li>

		          <?php
          		endwhile;
          endif;
          wp_reset_postdata();
        ?>
	</ul>
	<div class="pagination">
	  <?php
	     $big = 999999999; 
	      echo paginate_links( array(
	          'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
	          'current' => max( 1, get_query_var('paged') ),
	          'total' => $the_query_taxHoliday->max_num_pages,
	          'prev_text' => __ (''),
	          'next_text' => __ (''),
	      ) );
	     wp_reset_postdata();
	     ?>
	</div>
	</div>
	</main>
<?php get_footer(); ?>