
<?php get_header(); ?>
  <!--▼ Main ▼-->
  <main class="wrapper">
    <!-- Breacrum star -->
    <?php custom_breadcrumbs(); ?>
    <!-- Breacrum end -->
    <div class="container">
      <div class="listPost">
        <h2><?php the_title(); ?></h2>
        <span><?php echo get_the_date() .' - '. get_the_time(); ?></span>
        <div class="desc">
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
            the_content();
            endwhile; else: ?>
          <?php endif; ?>
        </div>  
        <div class="btn__pre">
          <a href="<?php echo esc_url( home_url( 'news' ) ); ?>">Back</a>
        </div>
     </div>
    </div>
  </main>
  <!--▲ Main ▲-->

<?php get_footer(); ?>