<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
  <!--▼ Main ▼-->
  <main class="wrapper">
    <!-- Mainvisual star -->
    <div class="index index__mainvisual">
      <!-- Item star --> 

            <?php
            while ( have_rows('banner') ) : the_row(); ?>
              <div class="index__item" style="background:url(<?php echo the_sub_field('slide_image'); ?>)">
                   <div class="index__info">
                      <h2>To travel is to live</h2>
                      <p>You Don't Need Magic to Disappear. All you need is a destination.</p>
                      <div class="index__btn">
                        <a href="#" class="index__btn--btn1">Show on the Map</a>
                        <a href="#" class="index__btn--btn2">More Info</a>
                      </div>
                    </div>
                  </div>  
            <?php endwhile; ?>
      <!-- Item end -->
    </div>
    <!-- Mainvisual end -->
    <!-- Index2 star -->
    <div class="index2">
      <div class="container">
        <!-- Index2 place star -->
        <div class="index2__place">
          <div class="index2__ttl">
            FIND YOUR <span>HOLYDAYS</span>
          </div>
          <div class="index2__inner">
            <div class="index2__box">
              <div class="index2__titleSmall">Where</div>
              <div class="index2__select">
                <select name="select">
                  <option>distination</option>
                  <option value="distination1">distination1</option>
                  <option value="distination2">distination2</option>
                  <option value="distination3">distination3</option>
                  <option value="distination4">distination4</option>
                </select>
              </div>
            </div>
            <div class="index2__box">
              <div class="index2__titleSmall">When</div>
              <div class="index2__group">
                <input type="text" class="index2__input" name="date" id="date" data-select="datepicker" placeholder="select date">
                <button type="button" class="index2__btnDate" data-toggle="datepicker"><i class="fa fa-calendar"></i></button>
              </div>
            </div>
            <a href="#" class="index2__btnSearch">Search</a>
          </div>
        </div>
        <!-- Index2 place end -->
      </div>
    </div>
    <!-- Index2 end -->
    <!-- Index3 star -->
    <section class="index3">
        <!-- Title star -->
      <div class="title">
        <h3 class="title__ttl">Special Offers</h3>
        <p class="title__txt">Best 2014 packages where people love to stay!</p>
      </div>
      <!-- Title end -->
      <!-- Index 3 content star -->
      <div class="index3__content">
        <?php
        $args1 = array(
          'post_type' => 'special', 
          'posts_per_page'=>999,
          'orderby'=>'date',
          'order'=>'DESC'
        );
        $the_query1 = new WP_Query( $args1 );

        if ( $the_query1->have_posts() ) :

        while ( $the_query1->have_posts() ) : $the_query1->the_post();

        ?>
        <div class="index3__item">
          <img src="<?php the_field('image'); ?>" alt="img" class="__img" />
          <div class="index3__info">
            <h3><?php the_title(); ?></h3>
            <span><?php the_field('text'); ?></span>
            <div class="index3__updown">
              <ul>
                <li><span><?php the_field('temperature'); ?></span></li>
                <li><span><?php the_field('price'); ?></span></li>
                <li><span><?php the_field('day'); ?></span></li>
                <li><span class="expires"><?php the_field('expires'); ?></span></li>
              </ul>
            </div>
          </div>
        </div>
        <?php

        endwhile;

        endif;

        wp_reset_postdata();
      ?>
      </div>
      <!-- Index 4 content end -->
    </section>
    <!-- Index3 end -->
    <!-- Index4 star -->
    <section class="index4">
      <div class="container">
        <!-- Title star -->
        <div class="title title--type02">
          <h3 class="title__ttl">Holidays Type</h3>
          <p class="title__txt">get explore your dream to travel the world!</p>
        </div>
        <!-- Title end -->
        <!-- Index 4 content star -->
        <div class="index4__content">
          <!-- Item star -->
          <?php
          $taxonomy = 'holiday_categories';
          $terms = get_terms($taxonomy); 
          if ( $terms && !is_wp_error( $terms ) ) :
          ?>
          <?php foreach ( $terms as $term ) { 
          ?>
          <div class="index4__item">
            <a href="<?php echo get_term_link($term); ?>">
              <div class="index4__img">
                 <img src="<?php echo get_field( 'image_categories', $term ); ?>" />
                 <img src="<?php echo get_field( 'image_categories2', $term ); ?>" class="index4__hover" />
              </div>
              <span class="index4__txt"><?php echo $term->name; ?></span>
            </a>
          </div>
          <?php } ?>
          <?php endif;?>
          <!-- Item end -->
        </div>
        <!-- Index 4 content end -->
      </div>
    </section>
    <!-- Index4 end -->
    <!-- Index5 star -->
    <section class="index5">
      <!-- Title star -->
      <div class="title">
        <h3 class="title__ttl">Happy Clients</h3>
        <p class="title__txt">what customer say about us and why love our services!</p>
      </div>
      <!-- Title end -->
      <!-- Index 5 content star -->
      <div class="index5__content">
        <?php
          $args2 = array(
            'post_type' => 'testimonials', 
            'posts_per_page'=>999,
            'orderby'=>'date',
            'order'=>'DESC'
          );
          $the_query2 = new WP_Query( $args2 );

          if ( $the_query2->have_posts() ) :

          while ( $the_query2->have_posts() ) : $the_query2->the_post();

          ?>
          <div class="index5__item">
            <?php
            while ( have_rows('item') ) : the_row(); ?>
              <p><?php echo the_sub_field('opinion'); ?></p>
              <h3><?php echo the_sub_field('name'); ?></h3>
              <span><?php echo the_sub_field('localtion'); ?></span>
            <?php endwhile; ?>
          </div>
          <?php

          endwhile;

          endif;

          wp_reset_postdata();
        ?>
      </div>
      <!-- Index 5 content end -->
    </section>
    <!-- Index5 end -->
    <section id="contact" class="contact">
      <div class="container">
        <h2 class="contact__ttl">
          <span class="contact__subcatch">Ask about Voyage</span>
          <span class="contact__catch">Voyageについての</span>
        </h2>
        <p class="contact__txt">
          製品の導入や価格については下記のフォームよりお問い合わせ下さい。
        </p>
        <form action="#" class="form" id="form">
          <?php echo do_shortcode('[contact-form-7 id="558" title="Contact form 1"]'); ?>
        </form>
      </div>
    </section>
  </main>
  <!--▲ Main ▲-->

<?php get_footer(); ?>