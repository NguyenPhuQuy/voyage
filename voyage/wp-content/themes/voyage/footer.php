  <!--▼ Footer ▼-->
  <footer class="footer">
    <div class="container">
      <!-- Footer inner star -->
      <div class="footer__inner">
        <!-- Footer newsLetter star -->
        <div class="footer__newsLetter">
          <h2 class="footer__ttl">Newsletter </h2>
          <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore.</p>
          <?php echo do_shortcode('[newsletter_form button_label="GO" class="footer__newsLetter__checkSub"]
            [newsletter_field name="email" label=""]
            [/newsletter_form]') ?>
        </div>
        <!-- Footer newsLetter end -->
        <!-- Footer latestNews star -->
        <div class="footer__latestNews">
          <h2 class="footer__ttl">Latest News </h2>
          <?php
              $args3 = array(
                'posts_per_page' => 2,
                'post_status' => 'publish',
              );
              $the_query3 = new WP_Query( $args3 );

              if ( $the_query3->have_posts() ) :

              while ( $the_query3->have_posts() ) : $the_query3->the_post();

              ?>
              <article class="footer__latestNews__listPost">
                <div class="footer__latestNews__img">
                  <img src="<?php the_field('imgpost') ?>" alt="img" />
                </div>
                <div class="footer__latestNews__info">
                  <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  <span><?php echo get_the_date() .' - '. get_the_time(); ?></span>
                </div>
              </article>

              <?php
              endwhile;
              endif;
              wp_reset_postdata();
            ?>
        </div>
        <!-- Footer latestNews end -->
        <!-- Footer tags star -->
        <div class="footer__tags">
          <h2 class="footer__ttl">Tags</h2>
          <?php $tags = get_tags();
            if($tags){
              foreach ($tags as $tag) {
              ?>
                  <a href=""><?php echo $tag->name ?></a>
                <?php
              }
            }
          ?>
        </div>
        <!-- Footer tags end -->
        <!-- Footer address star -->
        <div class="footer__address">
          <h2 class="footer__ttl">Address</h2>
          <p>DieSachbearbeiter Schönhauser Allee <br>
          167c,10435 Berlin Germany<br>
          <span>E-mail:</span> moin@blindtextgenerator.de</p>
          <div class="footer__address__listContact">
              <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
              <a href="https://www.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a>
              <a href="https://www.pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
        <!-- Footer address end -->
      </div>
      <!-- Footer inner end -->
      <!-- Footer navigation star -->
      <div class="footer__navi">
        <ul>
          <?php
            wp_nav_menu(array(
              'menu' => 'Menu_Bottom',
              'theme_location' => 'menu-bottom',
            ))
          ?>
        </ul>
        <p class="footer__coppyright"><small>Copyright @voyage. All Right Reserved.</small></p>
      </div>
      <!-- Footer navigation end -->
      <div class="btn__goTop">
        <i class="fa fa-arrow-up"></i>
      </div>
    </div>
  </footer>
  <!--▲ Footer ▲-->
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/jquery-3.3.1.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/slick.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/jquery.validationEngine.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/jquery.validationEngine-en.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/jquery.datepicker.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/common/js/functions.js"></script>
</body>
</html>